Source: vecgeom
Section: libs
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Stephan Lachnit <stephanlachnit@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 cmake,
 libxerces-c-dev,
 vc-dev [any-amd64 any-arm any-arm64 any-i386],
 veccore-dev,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/science-team/vecgeom
Vcs-Git: https://salsa.debian.org/science-team/vecgeom.git
Homepage: https://gitlab.cern.ch/VecGeom/VecGeom

Package: vecgeom-dev
Section: libdevel
Architecture: any
Multi-Arch: no
Depends:
 ${misc:Depends},
 libvecgeom-dev (= ${binary:Version}),
 libvgdml-dev (= ${binary:Version}),
 veccore-dev,
 vc-dev [any-amd64 any-arm any-arm64 any-i386],
 libxerces-c-dev,
Description: geometry library for particle-detector simulation - development files
 VecGeom is a vectorized geometry modeller library with hit-detection features
 as needed by particle detector simulation at the LHC and beyond.
 .
 This package includes the development files for VecGeom.

Package: libvecgeom-dev
Section: libdevel
Architecture: any
Multi-Arch: no
Depends:
 ${misc:Depends},
 libvecgeom1.2 (= ${binary:Version}),
 veccore-dev,
 vc-dev [any-amd64 any-arm any-arm64 any-i386],
Suggests:
 vecgeom-dev (= ${binary:Version}),
Breaks: vecgeom-dev (<< 1.1.19+dfsg-1)
Description: geometry library for particle-detector simulation - headers
 VecGeom is a vectorized geometry modeller library with hit-detection features
 as needed by particle detector simulation at the LHC and beyond.
 .
 This package includes the headers for libvecgeom.

Package: libvecgeom1.2
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 sse4.2-support [any-amd64],
Description: geometry library for particle-detector simulation
 VecGeom is a vectorized geometry modeller library with hit-detection features
 as needed by particle detector simulation at the LHC and beyond.

Package: libvgdml-dev
Section: libdevel
Architecture: any
Multi-Arch: no
Depends:
 ${misc:Depends},
 libvgdml1.2 (= ${binary:Version}),
 libvecgeom-dev (= ${binary:Version}),
 libxerces-c-dev,
Suggests:
 vecgeom-dev (= ${binary:Version}),
Breaks: vecgeom-dev (<< 1.1.19+dfsg-1)
Description: GDML interoperability library for VecGeom - headers
 VecGeom is a vectorized geometry modeller library with hit-detection features
 as needed by particle detector simulation at the LHC and beyond.
 .
 This package includes the headers for libvgdml.

Package: libvgdml1.2
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 sse4.2-support [any-amd64],
Description: GDML interoperability library for VecGeom
 VecGeom is a vectorized geometry modeller library with hit-detection features
 as needed by particle detector simulation at the LHC and beyond.
 .
 This package includes the GDML interoperability library for VecGeom.
